package com.project.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.project.dbconn.getDbConnection;
import com.project.model.Dashboard;
import com.project.model.ListHistory;
import com.project.model.ListTransaksi;
import com.project.model.PenjualanProduk;
import com.project.model.Product;
import com.project.model.Report;

@Repository
public class ReportDaoImpl implements ReportDao {
private static final Logger LOGGER = LoggerFactory.getLogger(ReportDaoImpl.class);
	
	Connection conn = null;
	PreparedStatement pSt = null;
	PreparedStatement pSt2 = null;
	PreparedStatement pSt3 = null;
	PreparedStatement pSt4 = null;
	CallableStatement callableStatement = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;

	@Value("${report.queryTotalTransaction}")
	String queryTotalTransaction;
	
	@Value("${report.queryListTransaksi}")
	String queryListTransaksi;
	
	@Value("${report.queryHistoryTrxId}")
	String queryHistoryTrxId;
	
	@Value("${report.queryGetUserHistory}")
	String queryGetUserHistory;
	
	@Value("${report.queryGetOmzetByMonth}")
	String queryGetOmzetByMonth;
	
	@Value("${report.queryDetailUserHistory}")
	String queryDetailUserHistory;
	
	@Value("${report.queryGetOmzetByDaily}")
	String queryGetOmzetByDaily;
	
	@Value("${report.queryGetOmzetByYear}")
	String queryGetOmzetByYear;
	
	@Value("${report.queryListProduct}")
	String queryListProduct;
	
	@Value("${report.queryGetListDateHarian}")
	String queryGetListDateHarian;
	
	@Value("${report.queryGetListDateBulanan}")
	String queryGetListDateBulanan;
	
	@Value("${report.queryGetListDateTahunan}")
	String queryGetListDateTahunan;
	
	@Value("${report.queryProductSalesHarian}")
	String queryProductSalesHarian;
	
	@Value("${report.queryProductSalesBulanan}")
	String queryProductSalesBulanan;
	
	@Value("${report.queryProductSalesTahunan}")
	String queryProductSalesTahunan;
	
	@Value("${report.queryGetCountPenjualanHariIni}")
	String queryGetCountPenjualanHariIni;
	
	@Value("${report.queryGetOmzetHanyaHariIni}")
	String queryGetOmzetHanyaHariIni;
	
	@Value("${report.queryGetProdukHariIni}")
	String queryGetProdukHariIni;
	
	@Value("${report.queryStokKurangDariLima}")
	String queryStokKurangDariLima;

	@Value("${report.queryTest}")
	String queryTest;
	
	@Override
	public List<ListHistory> listHistory(String merchantId, String startDate, String endDate) {
		/*String queryTotalTransaction = "SELECT sum(sumtotal) as subtotal"
										+ " FROM ("
										+ " SELECT trxid, tgl, SUM(total) as sumtotal"
										+ " FROM sales sa"
										+ " JOIN customer cu ON sa.customerid = cu.customerid"
										+ " JOIN product pr ON sa.productid = pr.productid"
										+ " JOIN merchant me ON sa.merchantid = me.merchantid"
										+ " WHERE (trunc(tgl) BETWEEN (trunc(TO_DATE(?, 'yyyy-mm-dd'))) AND trunc(TO_DATE(?, 'yyyy-mm-dd'))) AND sa.merchantid=?"
										+ " GROUP BY trxid, tgl"
										+ " )";*/
		
		/*String queryListTransaksi = "SELECT trxid, tgl, SUM(total) as total"
				+ " FROM sales sa"
				+ " JOIN customer cu ON sa.customerid = cu.customerid"
				+ " JOIN product pr ON sa.productid = pr.productid"
				+ " JOIN merchant me ON sa.merchantid = me.merchantid"
				+ " WHERE (trunc(tgl) BETWEEN trunc(TO_DATE(?, 'yyyy-mm-dd')) AND trunc(TO_DATE(?, 'yyyy-mm-dd'))) AND sa.merchantid=?"
				+ " GROUP BY trxid, tgl"
				+ " ORDER BY tgl DESC, trxid asc";*/
		
		List<ListHistory> listHistoryReport = new ArrayList<ListHistory>();
		List<ListTransaksi> listHistoryTransaksi = new ArrayList<ListTransaksi>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryTotalTransaction + " ...");
			pSt = conn.prepareStatement(queryTotalTransaction);
			LOGGER.info("Prepared Statement dengan isi " + queryTotalTransaction + " berhasil dijalankan ...");
			
			pSt.setString(1, startDate);
			pSt.setString(2, endDate);
			pSt.setString(3, merchantId);

			rs = pSt.executeQuery();

			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryListTransaksi + " ...");
			pSt2 = conn.prepareStatement(queryListTransaksi);
			LOGGER.info("Prepared Statement dengan isi " + queryListTransaksi + " berhasil dijalankan ...");
			
			pSt2.setString(1, startDate);
			pSt2.setString(2, endDate);
			pSt2.setString(3, merchantId);

			rs2 = pSt2.executeQuery();
	
			while(rs.next()){
				ListHistory historyReport = new ListHistory();
				
				historyReport.setTotalTrasaction(rs.getString("subtotal"));
				
				while(rs2.next()){
					ListTransaksi historyTransaksi = new ListTransaksi();
					
					historyTransaksi.setTrxId(rs2.getString("trxid"));
					historyTransaksi.setTrxDate(rs2.getString("tgl"));
					historyTransaksi.setTotalHargaTransaksi(rs2.getString("total"));
					
					listHistoryTransaksi.add(historyTransaksi);
					historyReport.setListTransaksi(listHistoryTransaksi);
				}
				
				listHistoryReport.add(historyReport);
			}
			LOGGER.info("Berhasil mendapatkan data report untuk tanggal (YYYY-MM-DD) " + startDate + " sampai tanggal " + endDate + " ...");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				
				if (pSt2 != null) {
					pSt2.close();
				}
				
				if (rs2 != null) {
					rs2.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listHistoryReport;
	}

	@Override
	public List<Report> historyTrxId(String merchantId, String trxId) {
		/*String query = "SELECT salesid, trxid, sa.merchantid as merchantid, cu.customername as customername, merchantname, total, to_char(tgl,'YYYY-MM-DD hh24:mi:ss') as tgl, pr.nama as nama, sa.qty as qty, hargast"
				+ " FROM sales sa"
				+ " JOIN customer cu ON sa.customerid = cu.customerid"
				+ " JOIN product pr ON sa.productid = pr.productid"
				+ " JOIN merchant me ON sa.merchantid = me.merchantid"
				+ " WHERE sa.merchantid = ? AND sa.trxid = ?"
				+ " ORDER BY tgl DESC, salesid asc";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryHistoryTrxId + " ...");
			
			pSt = conn.prepareStatement(queryHistoryTrxId);
			LOGGER.info("Prepared Statement dengan isi " + queryHistoryTrxId + " berhasil dijalankan ...");
			
			pSt.setString(1, merchantId);
			pSt.setString(2, trxId);
			rs = pSt.executeQuery();
	
			if(!rs.isBeforeFirst())
			{LOGGER.error("ERROR GATAU KENAPA");}
			else{
				while(rs.next()){
						Report report = new Report();
						
						report.setSalesId(rs.getString("salesid"));
						report.setTrxId(rs.getString("trxid"));
						report.setMerchantId(rs.getString("merchantid"));
						report.setNamaCustomer(rs.getString("customername"));
						report.setNamaMerchant(rs.getString("merchantname"));
						report.setTotal(rs.getString("total"));
						report.setTgl(rs.getString("tgl"));
						report.setNamaProduk(rs.getString("nama"));
						report.setQtySales(rs.getString("qty"));
						report.setHargaSatuan(rs.getString("hargast"));
						listReport.add(report);
					}
				LOGGER.info("Berhasil mendapatkan data report untuk merchant dengan merchantId " + merchantId + " ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}
	
	@Override
	public List<Report> getUserHistory(String userId, String endDate) {
		/*String query = "SELECT sa.merchantid as merchantid, merchantname, sum(total) as total, to_char(tgl,'YYYY-MM-DD') as tgl"
				+ " FROM sales sa"
				+ " JOIN merchant me ON sa.merchantid = me.merchantid"
				+ " WHERE (tgl BETWEEN (TO_DATE(?, 'yyyy-mm-dd')-30) AND TO_DATE(?, 'yyyy-mm-dd')+1) AND sa.customerid=?"
				+ " GROUP BY sa.merchantid, merchantname, to_char(tgl,'YYYY-MM-DD')"
				+ " ORDER BY tgl DESC";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetUserHistory + " ...");
			
			pSt = conn.prepareStatement(queryGetUserHistory);
			LOGGER.info("Prepared Statement dengan isi " + queryGetUserHistory + " berhasil dijalanakan ...");
			
			pSt.setString(1, endDate);
			pSt.setString(2, endDate);
			pSt.setString(3, userId);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				Report report = new Report();
				
//				report.setSalesId(rs.getString("salesid"));
//				report.setTrxId(rs.getString("trxid"));
				report.setMerchantId(rs.getString("merchantid"));
//				report.setNamaCustomer(rs.getString("customername"));
				report.setNamaMerchant(rs.getString("merchantname"));
				report.setTotal(rs.getString("total"));
				report.setTgl(rs.getString("tgl"));
//				report.setNamaProduk(rs.getString("nama"));
//				report.setQtySales(rs.getString("qty"));
//				report.setHargaSatuan(rs.getString("hargast"));
				listReport.add(report);
			}
			LOGGER.info("Berhasil mendapatkan data history user");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}

	@Override
	public List<Report> detailUserHistory(String userId, String merchantId, String date) {
		/*String query = "SELECT salesid, trxid, sa.merchantid as merchantid, cu.customername as customername, merchantname, total, to_char(tgl,'YYYY-MM-DD hh24:mi:ss') as tgl, pr.nama as nama, sa.qty as qty, hargast"
				+ " FROM sales sa"
				+ " JOIN customer cu ON sa.customerid = cu.customerid"
				+ " JOIN product pr ON sa.productid = pr.productid"
				+ " JOIN merchant me ON sa.merchantid = me.merchantid"
				+ " WHERE sa.customerid=? AND sa.merchantid = ? AND (tgl BETWEEN TO_DATE(?, 'yyyy-mm-dd') AND TO_DATE(?, 'yyyy-mm-dd')+1)"
				+ " ORDER BY tgl DESC, salesid asc";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryDetailUserHistory + " ...");
			
			pSt = conn.prepareStatement(queryDetailUserHistory);
			LOGGER.info("Prepared Statement dengan isi " + queryDetailUserHistory + " berhasil dijalanakan ...");
			
			pSt.setString(1, userId);
			pSt.setString(2, merchantId);
			pSt.setString(3, date);
			pSt.setString(4, date);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				Report report = new Report();
				
				report.setSalesId(rs.getString("salesid"));
				report.setTrxId(rs.getString("trxid"));
				report.setMerchantId(rs.getString("merchantid"));
				report.setNamaCustomer(rs.getString("customername"));
				report.setNamaMerchant(rs.getString("merchantname"));
				report.setTotal(rs.getString("total"));
				report.setTgl(rs.getString("tgl"));
				report.setNamaProduk(rs.getString("nama"));
				report.setQtySales(rs.getString("qty"));
				report.setHargaSatuan(rs.getString("hargast"));
				listReport.add(report);
			}
			LOGGER.info("Berhasil mendapatkan data report");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}
	
	@Override
	public List<Report> getOmzetByMonth(Report report) {
		/*String query = "select to_char(to_date(tanggal,'DD-MM-YY'),'DD') AS TANGGAL, sum(total) as total from"
				+ "("
				+ " select to_char(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MM-YY') AS tanggal, 0 AS total"
				+ " from dual connect by "
				+ " rownum <= last_day(to_date(?,'dd/mm/yyyy'))-(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1)+1"
				+ " UNION"
				+ " ("
				+ " select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MM-YY') AS tanggal, sum(total) as total"
				+ " from sales"
				+ " where merchantid = ?"
				+ " and to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MON') = to_char(to_date(?,'DD-MM-YYYY'),'MON')"
				+ " and to_char(tgl, 'YY') = to_char(to_date(?, 'DD-MM-YYYY'), 'yy')"
				+ " group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MM-YY')"
				+ " )"
				+ ")"
				+ " group by to_char(to_date(tanggal,'DD-MM-YY'),'DD') order by tanggal asc";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetOmzetByMonth + " ...");
			
			pSt = conn.prepareStatement(queryGetOmzetByMonth);
			LOGGER.info("Prepared Statement dengan isi " + queryGetOmzetByMonth + " berhasil dijalankan ...");
			String tampungBulan = report.getTgl();
			
			pSt.setString(1, report.getTgl());
			pSt.setString(2, report.getTgl());
			pSt.setString(3, report.getTgl());
			pSt.setString(4, report.getMerchantId());
			pSt.setString(5, report.getTgl());
			pSt.setString(6, report.getTgl());
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				report = new Report();
				
				report.setTgl(rs.getString("tanggal"));
				report.setTotal(rs.getString("total"));
				listReport.add(report);
			}
			String str[] = tampungBulan.split("-");
			String month = str[1];
			LOGGER.info("Berhasil mendapatkan data pada bulan " + month + " ...");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}
	
	@Override
	public List<Report> getOmzetByDaily(Report report) {
		/*String query = "select to_date(tanggal, 'DD-MON-YYYY') as tanggal, sum(total) as total, to_char(to_date(tanggal, 'DD-MON-YYYY'), 'DD-MON-YY') as tanggalOut from"
				+ "("
				+ " select to_char(to_date(?, 'DD-MM-YYYY') + rownum -1, 'DD-MON-YY') as tanggal, 0 as total"
				+ " from dual"
				+ " connect by level <= to_date(?, 'DD-MM-YYYY') - to_date(?, 'DD-MM-YYYY') + 1"
				+ " UNION"
				+ " ("
				+ " select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY') AS tanggal, sum(total) as total"
				+ " from sales"
				+ " where merchantid = ?"
				+ " and to_date(to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')"
				+ " between"
				+ " to_date(to_char(to_timestamp(?,'DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')"
				+ " and"
				+ " to_date(to_char(to_timestamp(?,'DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')"
				+ " group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY')"
				+ " )"
				+ ") group by to_date(tanggal, 'DD-MON-YYYY'), to_char(to_date(tanggal, 'DD-MON-YYYY'), 'DD-MON-YY') order by tanggal asc";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetOmzetByDaily + " ...");
			
			pSt = conn.prepareStatement(queryGetOmzetByDaily);
			LOGGER.info("Prepared Statement dengan isi " + queryGetOmzetByDaily + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getTglAwal());
			LOGGER.info("tanggal awal -> " + report.getTglAwal());
			pSt.setString(2, report.getTglAkhir());
			LOGGER.info("tanggal akhir -> " + report.getTglAkhir());
			pSt.setString(3, report.getTglAwal());
			pSt.setString(4, report.getMerchantId());
			pSt.setString(5, report.getTglAwal());
			pSt.setString(6, report.getTglAkhir());
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				report = new Report();
				
				report.setTgl(rs.getString("tanggalout"));
				report.setTotal(rs.getString("total"));
				listReport.add(report);
			}

			LOGGER.info("Berhasil mendapatkan data pada tanggal " + listReport.get(0).getTgl() + " sampai tanggal " + listReport.get(listReport.size()-1).getTgl() + " ...");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}
	
	@Override
	public List<Report> getOmzetByYear(Report report) {
		/*String query = "select tanggal, sum(total) as total from "
				+ "("
				+ " SELECT to_char(add_months(SYSDATE, (LEVEL-1 )),'MONTH') as tanggal, 0 as total"
				+ " FROM dual"
				+ " CONNECT BY LEVEL <= 12"
				+ " UNION"
				+ " ("
				+ " select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') AS tanggal, sum(total) as total"
				+ " from sales"
				+ " where merchantid = ?"
				+ " and to_char(tgl, 'YY') = to_char(to_date(?, 'DD-MM-YYYY'), 'yy')"
				+ " group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH')"
				+ " )"
				+ ") group by tanggal order by to_date(tanggal, 'MM') asc";*/
		
		List<Report> listReport = new ArrayList<Report>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetOmzetByYear + " ...");
			
			pSt = conn.prepareStatement(queryGetOmzetByYear);
			LOGGER.info("Prepared Statement dengan isi " + queryGetOmzetByYear + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getMerchantId());
			pSt.setString(2, report.getTgl());
			
			String tanggalBuatLogger = report.getTgl();
			rs = pSt.executeQuery();
			
			while(rs.next()){
				report = new Report();
				String bulan = "";
				
				if(rs.getString("tanggal").contains("JANUARY")){
					bulan = "Januari";
				}
				else if(rs.getString("tanggal").contains("FEBRUARY")) {
					bulan = "Februari";
				}
				else if(rs.getString("tanggal").contains("MARCH")) {
					bulan = "Maret";
				}
				else if(rs.getString("tanggal").contains("APRIL")) {
					bulan = "April";
				}
				else if(rs.getString("tanggal").contains("MAY")) {
					bulan = "Mei";
				}
				else if(rs.getString("tanggal").contains("JUNE")) {
					bulan = "Juni";
				}
				else if(rs.getString("tanggal").contains("JULY")) {
					bulan = "Juli";
				}
				else if(rs.getString("tanggal").contains("AUGUST")) {
					bulan = "Agustus";
				}
				else if(rs.getString("tanggal").contains("SEPTEMBER")) {
					bulan = "September";
				}
				else if(rs.getString("tanggal").contains("OCTOBER")) {
					bulan = "Oktober";
				}
				else if(rs.getString("tanggal").contains("NOVEMBER")) {
					bulan = "November";
				}
				else if(rs.getString("tanggal").contains("DECEMBER")) {
					bulan = "Desember";
				}				
				
				report.setTgl(bulan);
				report.setTotal(rs.getString("total"));
				listReport.add(report);
			}

			LOGGER.info("Berhasil mendapatkan data pada tahun " + tanggalBuatLogger + " ...");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listReport;
	}

	@Override
	public List<PenjualanProduk> getPenjualanHarian(PenjualanProduk pp) {
		/*String queryListDate = "select to_char(to_date(?, 'DD-MM-YYYY') + rownum -1, 'DD-MON-YY') as tanggal"
				+ " from dual"
				+ " connect by level <= to_date(?, 'DD-MM-YYYY') - to_date(?, 'DD-MM-YYYY') + 1";*/
		
		/*String queryListProduct ="SELECT productid, nama"
				+ " FROM product"
				+ " WHERE merchantid = ? and productid = ?  ORDER BY productid ASC";*/
		
		/*String queryProductSales = "select tanggal, sum(qty) as productSales from"
				+ "("
				+ " select to_char(to_date(?, 'DD-MM-YYYY') + rownum -1, 'DD-MON-YYyy') as tanggal, 0 as qty"
				+ " from dual"
				+ " connect by level <= to_date(?, 'DD-MM-YYYY') - to_date(?, 'DD-MM-YYYY') + 1"
				+ " UNION"
				+ " ("
				+ " select to_char(s.tgl, 'dd-MON-yyyy') as tanggal, s.qty"
				+ " from product p, merchant m, sales s"
				+ " where p.merchantid = m.merchantid and m.merchantid = s.merchantid and s.productid = p.productid"
				+ " and m.merchantid = ? and p.productid = ?"
				+ " and to_date(to_char(to_timestamp(s.tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'), 'yyyy-mm-dd'),'yyyy-mm-dd')"
				+ " between"
				+ " to_date(to_char(to_timestamp(?,'DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')"
				+ " and"
				+ " to_date(to_char(to_timestamp(?,'DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')"
				+ " )"
				+ ") group by tanggal order by tanggal asc";*/
		 
		List<PenjualanProduk> listPp = new ArrayList<PenjualanProduk>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			//mulai masukin ke ListDate
			pSt = conn.prepareStatement(queryGetListDateHarian);
			
			pSt.setString(1, pp.getStartDate());
			LOGGER.info("Tanggal awal -> " + pp.getStartDate() + " ...");
			pSt.setString(2, pp.getEndDate());
			LOGGER.info("Tanggal awal -> " + pp.getEndDate() + " ...");
			pSt.setString(3, pp.getStartDate());
			
			rs = pSt.executeQuery();
			
			List<String> listDate = new ArrayList<String>();
			List<Product> listProduct = new ArrayList<Product>();
			List<String> productSales = new ArrayList<String>();
			
			while(rs.next()){
				String tanggal = rs.getString("tanggal"); //17-NOV-18
				String str[] = tanggal.split("-"); //17  NOV  18
				String tgl = str[0];
				String month = str[1]; //
				String year = str[2];
				
				if(month.contains("MAY")) {
					month = "MEI";
				}
				else if(month.contains("AUG")) {
					month = "AGU";
				}
				else if(month.contains("OCT")) {
					month = "OKT";
				}
				else if(month.contains("DEC")) {
					month = "DES";
				}
				
				String tanggalfinal = tgl+"-"+month+"-"+year;
				
				listDate.add(tanggalfinal); //ngisi listDate: [2018-12-1, 2018-12-2, 2018-12-3, 2018-12-4, ….. 2018-12-30]
			}
			pp.setListDate(listDate); //masukin listDate ke dalam model
		for(int i = 0 ; i < pp.getListInputProductId().size() ; i ++) { //looping dari productid yang didapat
				pSt2 = conn.prepareStatement(queryListProduct);
				
				pSt2.setString(1, pp.getMerchantId());
				pSt2.setString(2, pp.getListInputProductId().get(i));
				rs2 = pSt2.executeQuery();
				
				while(rs2.next()) {
					Product product = new Product();
					product.setProductId(rs2.getString("productid"));
					product.setProductName(rs2.getString("nama"));
					pSt3 = conn.prepareStatement(queryProductSalesHarian); //query buat dapetin tanggal dan ngisi productSales
					
					pSt3.setString(1, pp.getStartDate());
					pSt3.setString(2, pp.getEndDate());
					pSt3.setString(3, pp.getStartDate());
					pSt3.setString(4, pp.getMerchantId());
					pSt3.setString(5, product.getProductId());
					pSt3.setString(6, pp.getStartDate());
					pSt3.setString(7, pp.getEndDate());
					rs3 = pSt3.executeQuery();
					
					productSales = new ArrayList<String>();
					while(rs3.next()) {
						productSales.add(rs3.getString("productSales")); 
					}
					product.setProductSales(productSales);
					listProduct.add(product);
				}
				pp.setListProduct(listProduct);
			}
			
			if(!pp.getListDate().isEmpty() && !pp.getListProduct().isEmpty()) {
				LOGGER.info("ListDate dan ListProduct tidak kosong ...");
				listPp.add(pp);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				
				if (pSt2 != null) {
					pSt2.close();
				}
				
				if (rs2 != null) {
					rs2.close();
				}
				
				if (rs3 != null) {
					rs3.close();
				}
				
				if (pSt3 != null) {
					pSt3.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listPp;
	}

	@Override
	public List<PenjualanProduk> getPenjualanBulanan(PenjualanProduk pp) {
		/*String queryListDate = "select to_char(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MM-YY') AS tanggal, 0 AS qty"
				+ " from dual"
				+ " connect by"
				+ " rownum <= last_day(to_date(?,'dd/mm/yyyy'))-(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1)+1";*/
		
		/*String queryListProduct ="SELECT productid, nama"
				+ " FROM product"
				+ " WHERE merchantid = ? and productid = ? ORDER BY productid ASC";*/
		
		/*String queryProductSales = "select tanggal, sum(qty) as productSales from"
				+ "("
				+ " select to_char(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MON-YYyy') AS tanggal, 0 AS qty"
				+ " from dual"
				+ " connect by"
				+ " rownum <= last_day(to_date(?,'dd/mm/yyyy'))-(last_day(add_months(to_date(?,'dd/mm/yyyy'), -1))+1)+1"
				+ " UNION"
				+ " ("
				+ " select to_char(s.tgl, 'dd-MON-yyyy') as tanggal, s.qty as qty"
				+ " from product p, merchant m, sales s"
				+ " where p.merchantid = m.merchantid and m.merchantid = s.merchantid and s.productid = p.productid"
				+ " and m.merchantid = ? and p.productid = ?"
				+ " and to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MON') = to_char(to_date(?,'DD-MM-YYYY'),'MON')"
				+ " and to_char(tgl, 'YY') = to_char(to_date(?, 'DD-MM-YYYY'), 'yy')"
				+ " )"
				+ ") group by tanggal order by tanggal";*/
		 
		List<PenjualanProduk> listPp = new ArrayList<PenjualanProduk>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			//mulai masukin ke ListDate
			pSt = conn.prepareStatement(queryGetListDateBulanan);
			
			pSt.setString(1, pp.getStartDate());
			pSt.setString(2, pp.getStartDate());
			pSt.setString(3, pp.getStartDate());
			LOGGER.info("Tanggal awal -> " + pp.getStartDate() + " ...");
			
			rs = pSt.executeQuery();
			
			//inisialisasi semua list
			List<String> listDate = new ArrayList<String>();
			List<Product> listProduct = new ArrayList<Product>();
			List<String> productSales = new ArrayList<String>();
			
			while(rs.next()){
				listDate.add(rs.getString("tanggal")); //ngisi listDate: [2018-12-1, 2018-12-2, 2018-12-3, 2018-12-4, ….. 2018-12-30]
			}
			pp.setListDate(listDate); //masukin listDate ke dalam model
			
			for(int i = 0 ; i < pp.getListInputProductId().size() ; i ++) { //looping dari productid yang didapat
				
				pSt2 = conn.prepareStatement(queryListProduct);
				
				pSt2.setString(1, pp.getMerchantId());
				pSt2.setString(2, pp.getListInputProductId().get(i));
				
				rs2 = pSt2.executeQuery();
				
				while(rs2.next()) {
				
					Product product = new Product();
					product.setProductId(rs2.getString("productid"));
					product.setProductName(rs2.getString("nama"));
					
					pSt3 = conn.prepareStatement(queryProductSalesBulanan); //query buat dapetin tanggal dan ngisi productSales
					
					pSt3.setString(1, pp.getStartDate());
					pSt3.setString(2, pp.getStartDate());
					pSt3.setString(3, pp.getStartDate());
					pSt3.setString(4, pp.getMerchantId());
					pSt3.setString(5, product.getProductId());
					pSt3.setString(6, pp.getStartDate());
					pSt3.setString(7, pp.getStartDate());
					
					rs3 = pSt3.executeQuery();
					
					productSales = new ArrayList<String>();
					while(rs3.next()) {
						productSales.add(rs3.getString("productSales")); 
					}
					product.setProductSales(productSales);
					listProduct.add(product);
				}
				pp.setListProduct(listProduct);
			}
			
			if(pp.getListDate() != null && pp.getListProduct() != null) {
				LOGGER.info("ListDate dan ListProduct tidak kosong ...");
				listPp.add(pp);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				
				if (pSt2 != null) {
					pSt2.close();
				}
				
				if (rs2 != null) {
					rs2.close();
				}
				
				if (rs3 != null) {
					rs3.close();
				}
				
				if (pSt3 != null) {
					pSt3.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listPp;
	}

	@Override
	public List<PenjualanProduk> getPenjualanTahunan(PenjualanProduk pp) {
		/*String queryListDate = "select to_char(add_months(trunc(sysdate, 'yyyy'), level - 1), 'MONTH') as tanggal, 0 as qty"
				+ " from dual"
				+ " connect by level <= 12";*/
		
		/*String queryListProduct ="SELECT productid, nama"
				+ " FROM product"
				+ " WHERE merchantid = ? and productid = ? ORDER BY productid ASC";*/
		
		/*String queryProductSales = "select tanggal, sum(qty) as productSales from"
				+ "("
				+ " select to_char(add_months(trunc(sysdate, 'yyyy'), level - 1), 'MONTH') as tanggal, 0 as qty"
				+ " from dual"
				+ " connect by level <= 12"
				+ " UNION"
				+ " ("
				+ " select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') AS tanggal, sum(qty) as qty"
				+ " from sales"
				+ " where merchantid = ? and productid = ?"
				+ " and to_char(tgl, 'YY') = to_char(to_date(?, 'DD-MM-YYYY'), 'yy')"
				+ " group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH')"
				+ " )"
				+ ") group by tanggal order by to_date(tanggal, 'MM') asc";*/
		 
		List<PenjualanProduk> listPp = new ArrayList<PenjualanProduk>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			//mulai masukin ke ListDate
			pSt = conn.prepareStatement(queryGetListDateTahunan);
			
			rs = pSt.executeQuery();
			
			//inisialisasi semua list
			List<String> listDate = new ArrayList<String>();
			List<Product> listProduct = new ArrayList<Product>();
			List<String> productSales = new ArrayList<String>();
			
			while(rs.next()){
				
				String bulan = "";
				
				if(rs.getString("tanggal").contains("JANUARY")){
					bulan = "Januari";
				}
				else if(rs.getString("tanggal").contains("FEBRUARY")) {
					bulan = "Februari";
				}
				else if(rs.getString("tanggal").contains("MARCH")) {
					bulan = "Maret";
				}
				else if(rs.getString("tanggal").contains("APRIL")) {
					bulan = "April";
				}
				else if(rs.getString("tanggal").contains("MAY")) {
					bulan = "Mei";
				}
				else if(rs.getString("tanggal").contains("JUNE")) {
					bulan = "Juni";
				}
				else if(rs.getString("tanggal").contains("JULY")) {
					bulan = "Juli";
				}
				else if(rs.getString("tanggal").contains("AUGUST")) {
					bulan = "Agustus";
				}
				else if(rs.getString("tanggal").contains("SEPTEMBER")) {
					bulan = "September";
				}
				else if(rs.getString("tanggal").contains("OCTOBER")) {
					bulan = "Oktober";
				}
				else if(rs.getString("tanggal").contains("NOVEMBER")) {
					bulan = "November";
				}
				else if(rs.getString("tanggal").contains("DECEMBER")) {
					bulan = "Desember";
				}			
				
				listDate.add(bulan); //ngisi listDate: [2018-12-1, 2018-12-2, 2018-12-3, 2018-12-4, ….. 2018-12-30]
			}
			pp.setListDate(listDate); //masukin listDate ke dalam model
			
			for(int i = 0 ; i < pp.getListInputProductId().size() ; i ++) { //looping dari productid yang didapat
				pSt2 = conn.prepareStatement(queryListProduct);
				
				pSt2.setString(1, pp.getMerchantId());
				pSt2.setString(2, pp.getListInputProductId().get(i));
				
				rs2 = pSt2.executeQuery();
				
				while(rs2.next()) {
				
					Product product = new Product();
					product.setProductId(rs2.getString("productid"));
					product.setProductName(rs2.getString("nama"));
					pSt3 = conn.prepareStatement(queryProductSalesTahunan); //query buat dapetin tanggal dan ngisi productSales
					
					pSt3.setString(1, pp.getMerchantId());
					pSt3.setString(2, product.getProductId());
					pSt3.setString(3, pp.getStartDate());
					
					rs3 = pSt3.executeQuery();
					
					productSales = new ArrayList<String>();
					while(rs3.next()) {
						productSales.add(rs3.getString("productSales")); 
					}
					product.setProductSales(productSales);
					listProduct.add(product);
				}
				pp.setListProduct(listProduct);
			}
			
			if(pp.getListDate() != null && pp.getListProduct() != null) {
				LOGGER.info("ListDate dan ListProduct tidak kosong ...");
				listPp.add(pp);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				
				if (pSt2 != null) {
					pSt2.close();
				}
				
				if (rs2 != null) {
					rs2.close();
				}
				
				if (rs3 != null) {
					rs3.close();
				}
				
				if (pSt3 != null) {
					pSt3.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listPp;
	}

	
	@Override
	public Integer getCountPenjualanHariIni(Report report) {
		Integer countjual = null;
		try{
			conn = getDbConnection.Access(); 
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetCountPenjualanHariIni + " ...");
			
			pSt = conn.prepareStatement(queryGetCountPenjualanHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetCountPenjualanHariIni + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getMerchantId());
			
			rs = pSt.executeQuery();

			while(rs.next()) {
				countjual = rs.getInt("jumlah");
			}

		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return countjual;
	}

	@Override
	public String getOmzetHanyaHariIni(Report report) {
		String totalOmzetHariIni = null;
		try{
			conn = getDbConnection.Access(); 
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetOmzetHanyaHariIni + " ...");
			
			pSt = conn.prepareStatement(queryGetOmzetHanyaHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetOmzetHanyaHariIni + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getMerchantId());
			
			rs = pSt.executeQuery();

			while(rs.next()) {
				totalOmzetHariIni = rs.getString("totalHariIni");
				
				if(totalOmzetHariIni == null) {
					totalOmzetHariIni = "0";
				}
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return totalOmzetHariIni;
	}

	@Override
	public List<Report> getProdukHariIni(Report report) {
		
		List<Report> listReport = new ArrayList<Report>();
		try{
			conn = getDbConnection.Access(); 
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetProdukHariIni + " ...");
			
			pSt = conn.prepareStatement(queryGetProdukHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetProdukHariIni + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getMerchantId());
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				report = new Report();
				LOGGER.error("Belum ada penjualan hari ini ...");
				report.setNamaProduk("Belum ada produk yang terjual");
				report.setQtySales("0");
				listReport.add(report);
			}
			else {
				while(rs.next()) {
					report = new Report();
					report.setNamaProduk(rs.getString("nama"));
					report.setQtySales(rs.getString("qty"));
					listReport.add(report);
				}
			}
			
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return listReport;
	}

	@Override
	public List<Report> getStokKurangDariLima(Report report) {
		List<Report> listReport = new ArrayList<Report>();
		try{
			conn = getDbConnection.Access(); 
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryStokKurangDariLima + " ...");
			
			pSt = conn.prepareStatement(queryStokKurangDariLima);
			LOGGER.info("Prepared Statement dengan isi " + queryStokKurangDariLima + " berhasil dijalankan ...");
			
			pSt.setString(1, report.getMerchantId());
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				report = new Report();
				LOGGER.error("Tidak ada produk yang kurang dari 5 ...");
				report.setNamaProduk("Stok semua produk masih diatas 5 pcs");
				report.setQtySales("99");
				listReport.add(report);
			}
			else {
				while(rs.next()) {
					report = new Report();
					report.setNamaProduk(rs.getString("nama"));
					report.setQtySales(rs.getString("qty"));
					listReport.add(report);
				}
			}
			
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return listReport;
	}

	@Override
	public Dashboard getDashboardData(Report report) {
		List<Report> listStokProduk = new ArrayList<Report>();
		List<Report> listProdukTerjual = new ArrayList<Report>();
		Dashboard dashboard = new Dashboard();
		String merchantid = report.getMerchantId();
		try{
			conn = getDbConnection.Access(); 
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryStokKurangDariLima + " ...");
			
			pSt = conn.prepareStatement(queryStokKurangDariLima);
			LOGGER.info("Prepared Statement dengan isi " + queryStokKurangDariLima + " berhasil dijalankan ...");
			
			pSt.setString(1, merchantid);
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				report = new Report();
				LOGGER.error("Tidak ada produk yang kurang dari 5 ...");
				report.setNamaProduk("Stok semua produk masih diatas 5 pcs");
				report.setQtySales("99");
				listStokProduk.add(report);
			}
			else {
				while(rs.next()) {
					report = new Report();
					report.setNamaProduk(rs.getString("nama"));
					report.setQtySales(rs.getString("qty"));
					LOGGER.info("stok kurang dari lima -> " + report.toString());
					listStokProduk.add(report);
				}
			}
			dashboard.setStokProdukKurangDariLima(listStokProduk);
			LOGGER.info("dashboard1 -> " + dashboard.toString());
			//PreparedStatement ProdukTerjualHariIni
			LOGGER.info("Siap menjalankan query " + queryGetProdukHariIni + " ...");
			
			pSt2 = conn.prepareStatement(queryGetProdukHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetProdukHariIni + " berhasil dijalankan ...");
			
			pSt2.setString(1, merchantid);
			LOGGER.info("merchantid di produk terlaris -> " + merchantid);
			rs2 = pSt2.executeQuery();
			
			if(!rs2.isBeforeFirst()) {
				report = new Report();
				LOGGER.error("Belum ada penjualan hari ini ...");
				report.setNamaProduk("Belum ada produk yang terjual");
				report.setQtySales("0");
				listProdukTerjual.add(report);
			}
			else {
				while(rs2.next()) {
					report = new Report();
					report.setNamaProduk(rs2.getString("nama"));
					report.setQtySales(rs2.getString("qty"));
					report.setStock(rs2.getString("stock"));
					LOGGER.info("Report Produk terlaris -> " + report.toString());
					listProdukTerjual.add(report);
				}
			}
			dashboard.setTotalProdukTerjualHariIni(listProdukTerjual);
			
			//PreparedStatement untuk dapetin omzet hari ini
			LOGGER.info("Siap menjalankan query " + queryGetOmzetHanyaHariIni + " ...");
			
			pSt3 = conn.prepareStatement(queryGetOmzetHanyaHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetOmzetHanyaHariIni + " berhasil dijalankan ...");
			
			pSt3.setString(1, merchantid);
			rs3 = pSt3.executeQuery();
			
			if(!rs3.isBeforeFirst()) {
				dashboard.setTotalOmzetHariIni("Ada kesalahan di server");
				LOGGER.error("ada kesalahan di query mungkin ...");
			}
			else {
				while(rs3.next()) {
					dashboard.setTotalOmzetHariIni(rs3.getString("totalHariIni"));
					
					if(dashboard.getTotalOmzetHariIni() == null) {
						dashboard.setTotalOmzetHariIni("0");
					}
				}
			}
			
			//PreparedStatement untuk dapetin countpenjualan hari ini
			LOGGER.info("Siap menjalankan query " + queryGetCountPenjualanHariIni + " ...");
			
			pSt4 = conn.prepareStatement(queryGetCountPenjualanHariIni);
			LOGGER.info("Prepared Statement dengan isi " + queryGetCountPenjualanHariIni + " berhasil dijalankan ...");
			
			pSt4.setString(1, merchantid);
			
			rs4 = pSt4.executeQuery();

			while(rs4.next()) {
				dashboard.setCountTransaksiHariIni(String.valueOf(rs4.getInt("jumlah")));
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}
				if (rs2 != null) {
					rs2.close();
				}
				if (rs3 != null) {
					rs3.close();
				}
				if (rs4 != null) {
					rs4.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				if (pSt2 != null) {
					pSt2.close();
				}
				if (pSt3 != null) {
					pSt3.close();
				}
				if (pSt4 != null) {
					pSt4.close();
				}
				
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return dashboard;
	}

	@Override
	public List<Report> testSelect() {
		
		List<Report> listReport = new ArrayList<Report>();
		Report report = new Report();
	try {	
		conn = getDbConnection.Access(); //Get Connection
		LOGGER.info("Membuka koneksi database ...");
		
		//PreparedStatement
		
		pSt = conn.prepareStatement(queryTest);
		LOGGER.info("Prepared Statement dengan isi " + queryTest + " berhasil dijalankan ...");
				
		rs = pSt.executeQuery();
		
		if(!rs.isBeforeFirst()) {
			report = new Report();
			report.setTgl("Data Kosong");
		}
		while(rs.next()){
			report = new Report();
			
			report.setTgl(rs.getString("tgl"));
			report.setTotal(rs.getString("total"));
			report.setSalesId(rs.getString("salesid"));
			listReport.add(report);
		}
		
	} catch(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	} finally {
		try{
			if (rs != null) {
				rs.close();
			}

			if (callableStatement != null) {
				callableStatement.close();
			}

			if (conn != null) {
				conn.close();
			}
			
			if (pSt != null) {
				pSt.close();
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	return listReport;
	}
}
