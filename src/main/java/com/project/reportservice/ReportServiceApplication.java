package com.project.reportservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.project.controller.ReportController;
import com.project.dao.ReportDao;
import com.project.model.Report;
import com.project.service.ReportService;

@SpringBootApplication
@ComponentScan(basePackageClasses = ReportController.class)
@ComponentScan(basePackageClasses = ReportService.class)
@ComponentScan(basePackageClasses = ReportDao.class)
@ComponentScan(basePackageClasses = Report.class)
public class ReportServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportServiceApplication.class, args);
	}
}
