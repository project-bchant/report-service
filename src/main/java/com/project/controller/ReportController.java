package com.project.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.project.model.Dashboard;
import com.project.model.ListHistory;
import com.project.model.PenjualanProduk;
import com.project.model.Report;
import com.project.service.ReportService;


@CrossOrigin(origins = "*")
@RestController
public class ReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	 @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	                registry.addMapping("/**").allowedOrigins("*");
	            }
	        };
	    }
	
	@Autowired
	ReportService reportService;
	
	/*@RequestMapping(value = "/getOmzetByMonth", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getOmzetByMonth(@RequestBody Report report) {
		LOGGER.info("Memanggil service getOmzetByMonth ... ");
		
		List<Report> listReport = this.reportService.getOmzetByMonth(report);
		
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getOmzetByDaily", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getOmzetByDaily(@RequestBody Report report) {
		LOGGER.info("Memanggil service getOmzetByDaily ... ");
		List<Report> listReport = this.reportService.getOmzetByDaily(report);
		
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }

	@RequestMapping(value = "/getOmzetByYear", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getOmzetByYear(@RequestBody Report report) {
		LOGGER.info("Memanggil service getOmzetByYear ... ");
		
		List<Report> listReport = this.reportService.getOmzetByYear(report);
		
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }*/
	
	/*@RequestMapping(value = "/getPenjualanHarian", method = RequestMethod.POST)
    public ResponseEntity<List<PenjualanProduk>> getPenjualanHarian(@RequestBody PenjualanProduk pp) {
		LOGGER.info("Memanggil service getPenjualanHarian ... ");
		
		List<PenjualanProduk> listPp = this.reportService.getPenjualanHarian(pp);

		return new ResponseEntity<List<PenjualanProduk>>(listPp, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getPenjualanBulanan", method = RequestMethod.POST)
    public ResponseEntity<List<PenjualanProduk>> getPenjualanBulanan(@RequestBody PenjualanProduk pp) {
		LOGGER.info("Memanggil service getPenjualanBulanan ... ");
		
		List<PenjualanProduk> listPp = this.reportService.getPenjualanBulanan(pp);
		
		return new ResponseEntity<List<PenjualanProduk>>(listPp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPenjualanTahunan", method = RequestMethod.POST)
    public ResponseEntity<List<PenjualanProduk>> getPenjualanTahunan(@RequestBody PenjualanProduk pp) {
		LOGGER.info("Memanggil service getPenjualanTahunan ... ");
		
		List<PenjualanProduk> listPp = this.reportService.getPenjualanTahunan(pp);
		
		return new ResponseEntity<List<PenjualanProduk>>(listPp, HttpStatus.OK);
    }*/
	
	@RequestMapping(value = "/testSelect", method = RequestMethod.GET)
    public ResponseEntity<List<Report>> testSelect() {
		
		List<Report> listReport = this.reportService.testSelect();
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getLaporanProduk", method = RequestMethod.POST)
    public ResponseEntity<List<PenjualanProduk>> getLaporanProduk(@RequestBody PenjualanProduk pp) {
		
		List<PenjualanProduk> listPp;
		
		if(pp.getType().equalsIgnoreCase("harian")) {
			LOGGER.info("Memanggil service getPenjualanHarian ... ");
			listPp = this.reportService.getPenjualanHarian(pp);
		}
		else if(pp.getType().equalsIgnoreCase("bulanan")) {
			LOGGER.info("Memanggil service getPenjualanBulanan ... ");
			listPp = this.reportService.getPenjualanBulanan(pp);
		}
		else if(pp.getType().equalsIgnoreCase("tahunan")) {
			LOGGER.info("Memanggil service getPenjualanTahunan ... ");
			listPp = this.reportService.getPenjualanTahunan(pp);
		}
		else {
			LOGGER.info("Ada error dari front end ... ");
			listPp = null;
		}
		return new ResponseEntity<List<PenjualanProduk>>(listPp, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getDashboardData", method = RequestMethod.POST)
    public ResponseEntity<Dashboard> getDashboardData(@RequestBody Report report) {
		LOGGER.info("Memanggil service getDashboardData ... ");
		
		Dashboard dashboard = this.reportService.getDashboardData(report);
		
		return new ResponseEntity<Dashboard>(dashboard, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getLaporanPendapatan", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getLaporanPendapatan(@RequestBody Report report) {
		
		List<Report> listReport;
		
		if(report.getTipe().equalsIgnoreCase("harian")) {
			LOGGER.info("Memanggil service getOmzetByDaily ... ");
			listReport = this.reportService.getOmzetByDaily(report);
		}
		else if(report.getTipe().equalsIgnoreCase("bulanan")) {
			LOGGER.info("Memanggil service getOmzetByMonth ... ");
			listReport = this.reportService.getOmzetByMonth(report);
		}
		else if(report.getTipe().equalsIgnoreCase("tahunan")) {
			LOGGER.info("Memanggil service getOmzetByYear ... ");
			listReport = this.reportService.getOmzetByYear(report);
		}
		else {
			LOGGER.info("Ada error dari front end ... ");
			listReport  = null;
		}
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }
	
/*	@RequestMapping(value = "/getCountPenjualanHariIni", method = RequestMethod.POST)
    public ResponseEntity<Integer> getCountPenjualanHariIni(@RequestBody Report report) {
		LOGGER.info("Memanggil service getCountPenjualanHariIni ... ");
		
		Integer countjual = this.reportService.getCountPenjualanHariIni(report);
		
		if(countjual != 0) {
			return new ResponseEntity<Integer>(countjual, HttpStatus.OK);
		}
		else {
			LOGGER.error("Tidak ada penjualan untuk hari ini ...");
			countjual = 0;
			return new ResponseEntity<Integer>(countjual, HttpStatus.OK);
		}
    }
	
	@RequestMapping(value = "/getOmzetHanyaHariIni", method = RequestMethod.POST)
    public ResponseEntity<String> getOmzetHanyaHariIni(@RequestBody Report report) {
		LOGGER.info("Memanggil service getOmzetHanyaHariIni ... ");
		
		String omzetHariIni = this.reportService.getOmzetHanyaHariIni(report);
		
		if(!omzetHariIni.equals("0")) {
			return new ResponseEntity<String>(omzetHariIni, HttpStatus.OK);
		}
		else {
			LOGGER.error("Omzet hari ini 0 ...");
			omzetHariIni = String.valueOf(0);
			return new ResponseEntity<String>(omzetHariIni, HttpStatus.OK);
		}
    }
	
	@RequestMapping(value = "/getTopProdukHariIni", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getProdukHariIni(@RequestBody Report report) {
		LOGGER.info("Memanggil service getProdukHariIni ... ");
		
		List<Report> reportProdukHariIni = this.reportService.getProdukHariIni(report);
		
		return new ResponseEntity<List<Report>>(reportProdukHariIni, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getStokKurangDariLima", method = RequestMethod.POST)
    public ResponseEntity<List<Report>> getStokKurangDariLima(@RequestBody Report report) {
		LOGGER.info("Memanggil service getStokKurangDariLima ... ");
		
		List<Report> reportProdukHariIni = this.reportService.getStokKurangDariLima(report);
		
		return new ResponseEntity<List<Report>>(reportProdukHariIni, HttpStatus.OK);
    }*/
	
	@RequestMapping(value = "/getUserHistory", method = RequestMethod.GET)
    public ResponseEntity<List<Report>> getUserHistory(@RequestHeader("UserID") String userId, 
    		@RequestHeader("EndDate") String endDate) {
		LOGGER.info("Memanggil service getUserHistory ... ");
		
		List<Report> listReport = this.reportService.getUserHistory(userId, endDate);
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/detailUserHistory", method = RequestMethod.GET)
    public ResponseEntity<List<Report>> detailUserHistory(@RequestHeader("UserID") String userId, @RequestHeader("MerchantID") String merchantId, 
    		@RequestHeader("Date") String date) {
		LOGGER.info("Memanggil service detailUserHistory ... ");
		
		List<Report> listReport = this.reportService.detailUserHistory(userId, merchantId, date);
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }

	@RequestMapping(value = "/listHistory", method = RequestMethod.GET) //history merchant, trx1 total harga 20k, trx2 total harga 30k
    public ResponseEntity<List<ListHistory>> listHistory(@RequestHeader("MerchantID") String merchantId, 
    	@RequestHeader("StartDate") String startDate, @RequestHeader("EndDate") String endDate) {
		LOGGER.info("Memanggil service listHistory ... ");
		
		List<ListHistory> listReport = this.reportService.listHistory(merchantId, startDate, endDate);
		return new ResponseEntity<List<ListHistory>>(listReport, HttpStatus.OK);
    }

	@RequestMapping(value = "/historyTrxId", method = RequestMethod.GET)//detail per mainTransaksi ada transaksi apa aja
    public ResponseEntity<List<Report>> historyTrxId(@RequestHeader("MerchantID") String merchantId, 
    		@RequestHeader("TrxID") String trxId) {
		LOGGER.info("Memanggil service historyTrxId ... ");
		List<Report> listReport = this.reportService.historyTrxId(merchantId, trxId);
		return new ResponseEntity<List<Report>>(listReport, HttpStatus.OK);
    }
}

