package com.project.model;

import java.util.ArrayList;
import java.util.List;

public class PenjualanProduk {
	private String startDate;
	private String endDate;
	private String merchantId;
	private List<String> listDate = new ArrayList<String>();
	private List<Product> listProduct = new ArrayList<Product>();
	private List<String> listInputProductId = new ArrayList<String>();
	private String type;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public List<String> getListDate() {
		return listDate;
	}
	public void setListDate(List<String> listDate) {
		this.listDate = listDate;
	}
	public List<Product> getListProduct() {
		return listProduct;
	}
	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	}
	
	public List<String> getListInputProductId() {
		return listInputProductId;
	}
	public void setListInputProductId(List<String> listInputProductId) {
		this.listInputProductId = listInputProductId;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "PenjualanProduk [startDate=" + startDate + ", endDate=" + endDate + ", merchantId=" + merchantId
				+ ", listDate=" + listDate + ", listProduct=" + listProduct + ", listInputProductId="
				+ listInputProductId + ", type=" + type + "]";
	}
	
	
}
