package com.project.model;

import java.util.ArrayList;
import java.util.List;

public class ListHistory {
	private String totalTrasaction;
	private List <ListTransaksi> listTransaksi = new ArrayList <ListTransaksi>();
	
    public String getTotalTrasaction() {
		return totalTrasaction;
	}
	public void setTotalTrasaction(String totalTrasaction) {
		this.totalTrasaction = totalTrasaction;
	}
	public List <ListTransaksi> getListTransaksi() {
		return listTransaksi;
	}
	public void setListTransaksi(List <ListTransaksi> listTransaksi) {
		this.listTransaksi = listTransaksi;
	}
}
