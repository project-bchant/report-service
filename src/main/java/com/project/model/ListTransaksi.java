package com.project.model;

public class ListTransaksi {
	private String trxId;
	private String trxDate;
	private String totalHargaTransaksi;
	
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	public String getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}
	public String getTotalHargaTransaksi() {
		return totalHargaTransaksi;
	}
	public void setTotalHargaTransaksi(String totalHargaTransaksi) {
		this.totalHargaTransaksi = totalHargaTransaksi;
	}
}
