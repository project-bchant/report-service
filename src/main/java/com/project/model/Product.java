package com.project.model;

import java.util.ArrayList;
import java.util.List;

public class Product {
	private String productId;
	private String productName;
	private List<String> productSales = new ArrayList<String>();
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<String> getProductSales() {
		return productSales;
	}
	public void setProductSales(List<String> productSales) {
		this.productSales = productSales;
	}


	
}
