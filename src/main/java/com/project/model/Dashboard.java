package com.project.model;

import java.util.List;

public class Dashboard {

	private String countTransaksiHariIni;
	private String totalOmzetHariIni;
	List<Report> totalProdukTerjualHariIni;
	List<Report> stokProdukKurangDariLima;
	
	public String getCountTransaksiHariIni() {
		return countTransaksiHariIni;
	}
	public void setCountTransaksiHariIni(String countTransaksiHariIni) {
		this.countTransaksiHariIni = countTransaksiHariIni;
	}
	public String getTotalOmzetHariIni() {
		return totalOmzetHariIni;
	}
	public void setTotalOmzetHariIni(String totalOmzetHariIni) {
		this.totalOmzetHariIni = totalOmzetHariIni;
	}
	public List<Report> getTotalProdukTerjualHariIni() {
		return totalProdukTerjualHariIni;
	}
	public void setTotalProdukTerjualHariIni(List<Report> totalProdukTerjualHariIni) {
		this.totalProdukTerjualHariIni = totalProdukTerjualHariIni;
	}
	public List<Report> getStokProdukKurangDariLima() {
		return stokProdukKurangDariLima;
	}
	public void setStokProdukKurangDariLima(List<Report> stokProdukKurangDariLima) {
		this.stokProdukKurangDariLima = stokProdukKurangDariLima;
	}
	@Override
	public String toString() {
		return "Dashboard [countTransaksiHariIni=" + countTransaksiHariIni + ", totalOmzetHariIni=" + totalOmzetHariIni
				+ ", totalProdukTerjualHariIni=" + totalProdukTerjualHariIni + ", stokProdukKurangDariLima="
				+ stokProdukKurangDariLima + "]";
	}
	
	
	
}
