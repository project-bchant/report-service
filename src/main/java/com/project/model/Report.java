package com.project.model;

public class Report {
	private String salesId;
	private String trxId;
	private String merchantId;
	private String namaCustomer;
	private String namaMerchant;
	private String total;
	private String tgl;
	private String namaProduk;
	private String qtySales;
	private String hargaSatuan;
	private String tglAwal;
	private String tglAkhir;
	private String stock;
	private String tipe;
	
	public String getSalesId() {
		return salesId;
	}
	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	public String getNamaCustomer() {
		return namaCustomer;
	}
	public void setNamaCustomer(String namaCustomer) {
		this.namaCustomer = namaCustomer;
	}
	public String getNamaMerchant() {
		return namaMerchant;
	}
	public void setNamaMerchant(String namaMerchant) {
		this.namaMerchant = namaMerchant;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	public String getNamaProduk() {
		return namaProduk;
	}
	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}
	public String getQtySales() {
		return qtySales;
	}
	public void setQtySales(String qtySales) {
		this.qtySales = qtySales;
	}
	public String getHargaSatuan() {
		return hargaSatuan;
	}
	public void setHargaSatuan(String hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}
	public String getTglAwal() {
		return tglAwal;
	}
	public void setTglAwal(String tglAwal) {
		this.tglAwal = tglAwal;
	}
	public String getTglAkhir() {
		return tglAkhir;
	}
	public void setTglAkhir(String tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	
	@Override
	public String toString() {
		return "Report [salesId=" + salesId + ", trxId=" + trxId + ", merchantId=" + merchantId + ", namaCustomer="
				+ namaCustomer + ", namaMerchant=" + namaMerchant + ", total=" + total + ", tgl=" + tgl
				+ ", namaProduk=" + namaProduk + ", qtySales=" + qtySales + ", hargaSatuan=" + hargaSatuan
				+ ", tglAwal=" + tglAwal + ", tglAkhir=" + tglAkhir + ", stock=" + stock + ", tipe=" + tipe + "]";
	}

	
}
