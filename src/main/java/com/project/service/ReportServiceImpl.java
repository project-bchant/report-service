package com.project.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.model.Dashboard;
import com.project.model.ListHistory;
import com.project.model.PenjualanProduk;
import com.project.controller.ReportController;
import com.project.dao.ReportDao;
import com.project.model.Report;

@Service
public class ReportServiceImpl implements ReportService{
	@Autowired
	ReportDao reportDao;

	@Override
	public List<ListHistory> listHistory(String merchantId, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return this.reportDao.listHistory(merchantId, startDate, endDate);
	}

	@Override
	public List<Report> historyTrxId(String merchantId, String trxId) {
		// TODO Auto-generated method stub
		return this.reportDao.historyTrxId(merchantId, trxId);
	}

	@Override
	public List<Report> getUserHistory(String userId, String endDate) {
		// TODO Auto-generated method stub
		return this.reportDao.getUserHistory(userId, endDate);
	}

	@Override
	public List<Report> getOmzetByMonth(Report report) {
		return this.reportDao.getOmzetByMonth(report);
	}

	@Override
	public List<Report> getOmzetByDaily(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getOmzetByDaily(report);
	}

	@Override
	public List<Report> getOmzetByYear(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getOmzetByYear(report);
	}

	@Override
	public List<PenjualanProduk> getPenjualanHarian(PenjualanProduk pp) {
		// TODO Auto-generated method stub
		return this.reportDao.getPenjualanHarian(pp);
	}

	@Override
	public List<PenjualanProduk> getPenjualanBulanan(PenjualanProduk pp) {
		// TODO Auto-generated method stub
		return this.reportDao.getPenjualanBulanan(pp);
	}

	@Override
	public List<PenjualanProduk> getPenjualanTahunan(PenjualanProduk pp) {
		// TODO Auto-generated method stub
		return this.reportDao.getPenjualanTahunan(pp);
	}

	@Override
	public List<Report> detailUserHistory(String userId, String merchantId, String date) {
		// TODO Auto-generated method stub
		return this.reportDao.detailUserHistory(userId, merchantId, date);
	}

	@Override
	public Integer getCountPenjualanHariIni(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getCountPenjualanHariIni(report);
	}

	@Override
	public String getOmzetHanyaHariIni(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getOmzetHanyaHariIni(report);
	}

	@Override
	public List<Report> getProdukHariIni(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getProdukHariIni(report);
	}

	@Override
	public List<Report> getStokKurangDariLima(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getStokKurangDariLima(report);
	}

	@Override
	public Dashboard getDashboardData(Report report) {
		// TODO Auto-generated method stub
		return this.reportDao.getDashboardData(report);
	}

	@Override
	public List<Report> testSelect() {
		// TODO Auto-generated method stub
		return this.reportDao.testSelect();
	}
}
