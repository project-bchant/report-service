package com.project.service;

import java.util.List;

import com.project.model.Dashboard;
import com.project.model.ListHistory;
import com.project.model.PenjualanProduk;
import com.project.model.Report;

public interface ReportService {
	List<ListHistory> listHistory(String merchantId, String startDate, String endDate);
	List<Report> historyTrxId(String merchantId, String trxId);
	List<Report> getUserHistory(String userId, String endDate);
	List<Report> detailUserHistory(String userId, String merchantId, String date);
	
	//ian
	List<Report> getOmzetByMonth(Report report);
	List<Report> getOmzetByDaily(Report report);
	List<Report> getOmzetByYear(Report report);
	List<PenjualanProduk> getPenjualanHarian(PenjualanProduk pp);
	List<PenjualanProduk> getPenjualanBulanan(PenjualanProduk pp);
	List<PenjualanProduk> getPenjualanTahunan(PenjualanProduk pp);
	Integer getCountPenjualanHariIni(Report report);
	String getOmzetHanyaHariIni(Report report);
	List<Report> getProdukHariIni (Report report);
	List<Report> getStokKurangDariLima (Report report);
	Dashboard getDashboardData (Report report);
	List<Report> testSelect();
}
