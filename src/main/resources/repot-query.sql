/*UNTUK QUERY AMBIL TANGGAL DI BULAN TERSEBUT*/
select to_char(to_date(tanggal,'DD-MM-YY'),'DD') AS TANGGAL, sum(total) as total from(
    select to_char(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MM-YY') AS tanggal, 0 AS total
    from dual 
    connect by 
    rownum <= last_day(to_date('05/11/2018','dd/mm/yyyy'))-(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1)+1
        UNION
    (select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MM-YY') AS tanggal, sum(total) as total from sales
    where merchantid = '1' 
    and to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MON') = to_char(to_date('05/11/2018','DD-MM-YYYY'),'MON')
    and to_char(tgl, 'YY') = to_char(to_date('05/11/2018', 'DD-MM-YYYY'), 'yy')
    group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MM-YY')) 
) group by to_char(to_date(tanggal,'DD-MM-YY'),'DD') order by tanggal asc;

/*UNTUK QUERY AMBIL TANGGAL DI ANTARA TANGGAL TERSEBUT*/
select to_char(to_date(tanggal, 'DD-MM-YYYY'), 'DD-MON-YYYY') as tanggal, sum(total) as total from
(
select to_char(to_date('17-11-2018', 'DD-MM-YYYY') + rownum -1, 'DD-MON-YY') as tanggal, 0 as total 
from dual 
connect by level <= to_date('24-11-2018', 'DD-MM-YYYY') - to_date('17-11-2018', 'DD-MM-YYYY') + 1
UNION
    (
    select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY') AS tanggal, sum(total) as total
    from sales
    where merchantid = '1'
    and to_date(to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY') 
    between 
        to_date(to_char(to_timestamp('17-11-2018','DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY') 
        and 
        to_date(to_char(to_timestamp('24-11-2018','DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')
    group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY')
    ) 
) group by to_char(to_date(tanggal, 'DD-MM-YYYY'), 'DD-MON-YYYY') order by tanggal asc;

/*QUERY UNTUK AMBIL BULAN DI DI TAHUN TERSEBUT*/
select tanggal, sum(total) as total from(
SELECT to_char(add_months(SYSDATE, (LEVEL-1 )),'MONTH') as tanggal, 0 as total
FROM dual 
CONNECT BY LEVEL <= 12
UNION
(
select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') AS tanggal, sum(total) as total
from sales
where merchantid = '1'
and to_char(tgl, 'YY') = to_char(to_date('05/11/2018', 'DD-MM-YYYY'), 'yy')
group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') 
)
) group by tanggal order by to_date(tanggal, 'MM') asc;


/*QUERY UNTUK DAPETIN LISTDATE*/
select to_char(to_date('17-11-2018', 'DD-MM-YYYY') + rownum -1, 'DD-MON-YY') as tanggal
from dual 
connect by level <= to_date('24-11-2018', 'DD-MM-YYYY') - to_date('17-11-2018', 'DD-MM-YYYY') + 1;

/*QUERY UNTUK DAPETIN LISTPRODUCT*/
SELECT productid, nama FROM product WHERE merchantid = '1' ORDER BY productid ASC;

/*QUERY UNTUK DAPETIN PRODUCTSALES , NANTI TGL NY D HAPUS*/
select tanggal, sum(qty) as productSales from(
select to_char(to_date('17-11-2018', 'DD-MM-YYYY') + rownum -1, 'DD-MON-YYyy') as tanggal, 0 as qty
from dual 
connect by level <= to_date('24-11-2018', 'DD-MM-YYYY') - to_date('17-11-2018', 'DD-MM-YYYY') + 1
    UNION(
        select to_char(s.tgl, 'dd-MON-yyyy') as tanggal, s.qty
        from product p, merchant m, sales s
        where p.merchantid = m.merchantid and m.merchantid = s.merchantid and s.productid = p.productid
        and m.merchantid = '1' and p.productid = '1'
        and to_date(to_char(to_timestamp(s.tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'), 'yyyy-mm-dd'),'yyyy-mm-dd')
        between 
            to_date(to_char(to_timestamp('17-11-2018','DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY') 
            and 
            to_date(to_char(to_timestamp('24-11-2018','DD-mm-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'DD-MON-YY'), 'DD-MM-YYYY')
    )
) group by tanggal order by tanggal asc;

/*QUERY UNTUK DAPETIN LISTDATE*/
select to_char(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MM-YY') AS tanggal, 0 AS qty
    from dual 
    connect by 
    rownum <= last_day(to_date('05/11/2018','dd/mm/yyyy'))-(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1)+1;
   
/*QUERY UNTUK DAPETIN LISTPRODUCT*/ 
SELECT productid, nama FROM product WHERE merchantid = '1' and productid = '1' ORDER BY productid ASC;

/*QUERY UNTUK DAPETIN PRODUCTSALES , NANTI TGL NY D HAPUS*/
select tanggal, sum(qty) as productSales from(
select to_char(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1+rownum-1, 'DD-MON-YYyy') AS tanggal, 0 AS qty
    from dual 
    connect by 
    rownum <= last_day(to_date('05/11/2018','dd/mm/yyyy'))-(last_day(add_months(to_date('05/11/2018','dd/mm/yyyy'), -1))+1)+1
UNION(
select to_char(s.tgl, 'dd-MON-yyyy') as tanggal, s.qty as qty
        from product p, merchant m, sales s
        where p.merchantid = m.merchantid and m.merchantid = s.merchantid and s.productid = p.productid
        and m.merchantid = '1' and p.productid = '1'
        and to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MON') = to_char(to_date('05/11/2018','DD-MM-YYYY'),'MON')
        and to_char(tgl, 'YY') = to_char(to_date('05/11/2018', 'DD-MM-YYYY'), 'yy')
)) group by tanggal order by tanggal;

/*QUERY UNTUK DAPETIN LISTDATE*/
select to_char(add_months(trunc(sysdate, 'yyyy'), level - 1), 'MONTH') as tanggal, 0 as qty
  from dual
connect by level <= 12;

/*QUERY UNTUK DAPETIN LISTPRODUCT*/ 
SELECT productid, nama FROM product WHERE merchantid = '1' and productid = '1' ORDER BY productid ASC;

/*QUERY UNTUK DAPETIN PRODUCTSALES , NANTI TGL NY D HAPUS*/
select tanggal, sum(qty) as productSales from(
select to_char(add_months(trunc(sysdate, 'yyyy'), level - 1), 'MONTH') as tanggal, 0 as qty
  from dual
connect by level <= 12
UNION
(
select to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') AS tanggal, sum(qty) as qty
from sales
where merchantid = '1' and productid = '1'
and to_char(tgl, 'YY') = to_char(to_date('05/11/2018', 'DD-MM-YYYY'), 'yy')
group by to_char(to_timestamp(tgl,'DD-MON-YYYY HH:MI:SS.FF9 PM','nls_date_language = ENGLISH'),'MONTH') 
)
) group by tanggal order by to_date(tanggal, 'MM') asc;